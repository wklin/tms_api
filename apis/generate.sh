cd apis
for dir in */; do
   for filename in ${dir}/*.mmd; do
       [ -f "$filename" ] || break
       basename=$(basename "$filename");
       mmdc -i $dir$basename -o ../public/img/${basename%.*}.png
   done
done

cd ..

