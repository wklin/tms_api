/**
 * @apiDefine NoContentSuccess
 * 
 * @apiSuccess (Success 204) 204 If successful, this method returns an empty response body.
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 204 No Content
 */

/**
 * @apiDefine BadRequestError
 *
 * @apiError 400 If request is malformed.
 * @apiErrorExample {json} Error-Response: 400
 *   HTTP/1.1 400 Bad Request
 *   {
 *      "error": 400,
 *      "message": "Invalid request parameters"
 *  }
 */

/**
 * @apiDefine UnauthorizedError
 *
 * @apiError 401 If request is not authorized
 * @apiErrorExample {json} Error-Response: 401
 *   HTTP/1.1 401 Unauthorized
 *   {
 *      "timestamp": "2018-08-23T07:57:39.823",
 *      "status": 401,
 *      "error": "Unauthorized",
 *      "message": "Unauthorized",
 *   }
 */

/**
 * @apiDefine PermissionDeniedError
 *
 * @apiError 403 If principal doesn't have permissions
 */

/**
 * @apiDefine DeviceNotFoundError
 *
 * @apiError 404 If device is not found
 */

/**
 * @apiDefine NetworkNotFoundError
 *
 * @apiError 404 If network is not found
 */

/**
 * @apiDefine UserNotFoundError
 *
 * @apiError 404 If user is not found
 */

/**
 * @apiDefine UserOrNetworkNotFoundError
 *
 * @apiError 404 If user or network not found.
 */

/**
 * @apiDefine CommandNotFoundError
 *
 * @apiError 404 If command not found.
 */

/**
 * @apiDefine DeviceOrNotificationNotFoundError
 *
 * @apiError 404 If device or notification not found
 */

/**
 * @apiDefine RuleNotFoundError
 *
 * @apiError 404 If rule not found.
 */

/**
 * @apiDefine NotSignedInError
 *
 * @apiError 409 If user is not signed in
 */

 /**
 * @apiDefine AppExistError
 *
 * @apiError 410 The APP already exists.
 */

 /**
  * @apiDefine VersionExistError
  * 
  * @apiError 411 The version already exists.
  */

 /**
  * @apiDefine AppNotFoundError
  * 
  * @apiError 412 If the app not found
  */

/**
 * @apiDefine VersionNotFoundError
 *
 * @apiError 413 If the given version not found
 */

/**
 * @apiDefine OTAFileNotFoundError
 *
 * @apiError 414 If OTA file not exist while configuring as active.
 */

 /**
 * @apiDefine NoUpdateError
 *
 * @apiError 415 If there is no update for the app.
 */