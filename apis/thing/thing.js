/**
 * @api {get} /v1/things Get Thing Model List
 * @apiVersion 1.0.0
 * @apiName getThingModelList
 * @apiGroup thing
 * @apiPermission GET_THING
 * @apiDescription
 * Get list of thing models.
 *
 * @apiExample {http} Example usage:
 *    GET /v1/things?limit=20&offset=1
 *    Content-Type: application/json
 * 
 * @apiHeader {string} Authorization The Bearer Token
 * @apiHeader {string} Content-Type The media type of the resource. It should be "application/json".
 *
 * @apiParam {number} [limit=20] Max records to return.
 * @apiParam {number} [offset=1] Requested starting record index.
 *
 * @apiSuccess {number} total Total number of records.
 * @apiSuccess {number} count Returned device count.
 * @apiSuccess {object[]} data List of returned records.
 * @apiSuccess {number} data.offset The index of the record.
 * @apiSuccess {object} data.thing_doc  A JSON object with thing model details.
 * @apiSuccess {string} data.thing_doc.pid  thing model pid
 * @apiSuccess {string} data.thing_doc.ppid thing model parent pid (inherited from)
 * @apiSuccess {number} data.thing_doc.created creation date
 * @apiSuccess {number} data.thing_doc.last_modified last modified date
 * @apiSuccess {string} data.thing_doc.md5sum md5 sum of the thing model
 * @apiSuccess {string} data.thing_doc.description thing model description
 * @apiSuccess {string} data.thing_doc.author thing model author
 * @apiSuccess {string} data.thing_doc.company company name
 * @apiSuccess {string} data.thing_doc.thing Thing model in json format
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *    Content-Type: application/json
 *    {
 *       "total": 20,
 *       "count": 2,
 *       "data": [
 *          {
 *             "offset": 1,
 *             "thing_doc": {
 *                 "pid": "30000000001",
 *                 "ppid": "",
 *                 "created": 1582714575,
 *                 "last_modified": 1582714575,
 *                 "md5sum": "74ed4d4d239732407e019812054a6458",
 *                 "description": "an IMS thing model",
 *                 "author": "william lin",
 *                 "company": "A good company",
 *                 "thing": {
 *                    ...
 *                }
 *             }
 *          }, {
 *             "offset": 2,
 *             "thing_doc": {
 *                 "pid": "40000000001",
 *                 "ppid": "",
 *                 "created": 1582714575,
 *                 "last_modified": 1582714575,
 *                 "md5sum": "74ed4d4d239732407e019812054a6458",
 *                 "description": "a XXX thing model",
 *                 "author": "wklin",
 *                 "company": "A company",
 *                 "thing": {
 *                    ...
 *                 }
 *             }
 *          }
 *       ]
 *    }
 *
 * @apiUse BadRequestError
 * @apiUse UnauthorizedError
 * @apiUse PermissionDeniedError
 */
  
/**
 * @api {get} /v1/things/{pid} Get Thing Model Info
 * @apiVersion 1.0.0
 * @apiName getThingModelInfo
 * @apiGroup thing
 * @apiPermission GET_THING
 * @apiDescription
 * Get thing model info of the specified pid.
 * 
 * @apiExample {http} Example usage:
 *    GET /v1/things/30000000001
 *    Content-Type: application/json
 * 
 * @apiHeader {string} Authorization The Bearer Token.
 * @apiHeader {string} Content-Type The media type of the resource. It should be "application/json".
 * 
 * @apiParam {string} pid The pid of the thing model.
 *
 * @apiSuccess {string} pid  thing model pid
 * @apiSuccess {string} ppid thing model parent pid (inherited from)
 * @apiSuccess {number} created creation date
 * @apiSuccess {number} last_modified last modified date
 * @apiSuccess {string} md5sum md5 sum of the thing model
 * @apiSuccess {string} description thing model description
 * @apiSuccess {string} author thing model author
 * @apiSuccess {string} company company name
 * @apiSuccess {string} thing Thing model in json format
 * @apiSuccess {string} thing.version thing model version
 * @apiSuccess {string} thing.schema thing model schema
 * @apiSuccess {string} thing.profile product profile
 * @apiSuccess {string} thing.profile.pid pid
 * @apiSuccess {string} thing.profile.ppid parent pid
 * @apiSuccess {object[]} thing.properties properties of the thing model
 * @apiSuccess {object[]} thing.services services of the thing model
 * @apiSuccess {object[]} thing.events events of the thing model
 * @apiSuccess {object[]} thing.device_info device info of the product

 * @apiSuccessExample {json} Success-Response:
 *  HTTP/1.1 200 OK
 *  Content-Type: application/json
 *  {
 *      "pid": "30000000001",
 *      "ppid": "",
 *      "created": 1582714575,
 *      "last_modified": 1582714575,
 *      "md5sum": "74ed4d4d239732407e019812054a6458",
 *      "description": "an IMS thing model",
 *      "author": "william lin",
 *      "company": "A good company",
 *      "thing": {
 *          "version": "v1",
 *          "schema": "https://corepro.fiibeacon.com/corepro_thing_schema.json",
 *          "profile": {
 *              "pid": "6456559484229801863",
 *              "ppid": "9876532333209876136"
 *          },
 *          "properties": [
 *              {
 *                  "identifier": "attribute1",
 *                  "display_name": "attribute1_display_name",
 *                  "operation": "rw",
 *                  "description": "descrption of attribute1",
 *                  "data_type": {
 *                      "type": "integer",
 *                      "min": 1,
 *                      "max": 10,
 *                      "unit": "cm"
 *                  }
 *              },
 *              {
 *                  "identifier": "attribute2",
 *                  "display_name": "attribute2_display_name",
 *                  "operation": "r",
 *                  "description": "description of attribute2",
 *                  "data_type": {
 *                      "type": "array",
 *                      "array_size": 1024,
 *                      "array_type": "integer"
 *                  }
 *              },
 *              {
 *                  "identifier": "attribute3",
 *                  "display_name": "attribute3_display_name",
 *                  "operation": "r",
 *                  "description": "description of attribute3",
 *                  "data_type": {
 *                      "type": "enum",
 *                      "enumeration": ["red", "green", "blue"]
 *                  }
 *              },
 *              {
 *                  "identifier": "attribute4",
 *                  "display_name": "attribute4_display_name",
 *                  "operation": "r",
 *                  "description": "description of attribute4",
 *                  "data_type": {
 *                      "type": "struct",
 *                      "struct_defs": [
 *                          {
 *                              "id": "name",
 *                              "type": "string"
 *                          }, {
 *                              "id": "birthday",
 *                              "type": "date"
 *                          }
 *                      ]
 *                  }
 *              }
 *          ],
 * 
 *          "services": [
 *              {
 *                  "identifier": "service1",
 *                  "display_name": "service1_display_name", 
 *                  "description": "description of service1",
 *                  "input_data": [
 *                      {
 *                          "identifier": "in_param1",
 *                          "description": "description of in_param1",
 *                          "display_name": "in_param1_display_name",
 *                          "data_type": {
 *                              "type": "integer",
 *                              "min": 1,
 *                              "max": 10,
 *                              "unit": "cm"
 *                          }
 *                      },
 *                      {
 *                          "identifier": "in_param2",
 *                          "display_name": "in_param2_display_name",
 *                          "description'": "description of in_param2",
 *                          "data_type": {
 *                              "type": "array",
 *                              "array_size": 16,
 *                              "array_type": "float"
 *                          }
 *                      }
 *                  ],
 *                  "output_data": [
 *                      {
 *                          "identifier": "out_param",
 *                          "display_name": "out_param_display_name",
 *                          "description": "description of out_param",
 *                          "data_type": {
 *                              "type": "float",
 *                              "min": 2.0,
 *                              "max": 5.0,
 *                              "unit": "inch"
 *                          }
 *                      }
 *                  ]
 *              }
 *          ],
 * 
 *          "events": [
 *              {
 *                  "identifier": "event1",
 *                  "display_name": "event1_display_name",
 *                  "description": "description of event1",
 *                  "event_type": "information",
 *                  "output_data": [
 *                      {
 *                          "identifier": "out_param1",
 *                          "display_name": "out_param1_display_name",
 *                          "description": "description of out_param1",
 *                          "data_type": {
 *                              "type": "integer",
 *                              "min": 10,
 *                              "max": 100,
 *                              "unit": "pa"
 *                          }
 *                      },
 *                      {
 *                          "identifier": "out_param2",
 *                          "display_name": "out_param2_display_name",
 *                          "description": "description of out_param2",
 *                          "data_type": {
 *                              "type": "string"
 *                          }
 *                      }
 *                  ]
 *              }
 *          ],
 *          "device_info": [
 *              {
 *                  "device_info_id": "status",
 *                  "device_info_type": {
 *                      "type": "enum",
 *                      "enumeration": [0, 1, 2]
 *                  }
 *              },
 *              {
 *                  "device_info_id": "cpu",
 *                  "device_info_type": {
 *                      "type": "integer",
 *                      "min": 0,
 *                      "max": 100
 *                  }
 *              },
 *              {
 *                  "device_info_id": "memory",
 *                  "device_info_type": {
 *                      "type": "integer",
 *                      "min": 0,
 *                      "unit": "MB"
 *                  }
 *              },
 *              {
 *                  "device_info_id": "disk",
 *                  "device_info_type": {
 *                      "type": "integer",
 *                      "min": 0,
 *                      "unit": "GB"
 *                  }
 *              }
 *          ]
 *      }
 *  }
 * 
 * @apiUse BadRequestError
 * @apiUse UnauthorizedError
 * @apiUse PermissionDeniedError
 */

/**
 * @api {post} /v1/things Register Thing Model
 * @apiVersion 1.0.0
 * @apiName registerThing
 * @apiGroup thing
 * @apiPermission MANAGE_THING
 * @apiDescription
 * Register a new thing model
 *
 * @apiExample {http} Example usage:
 *  POST /v1/things
 *  Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6.....
 *  Content-Type: application/json
 *  {
 *      "pid": "6456559484229801863"
 *      "ppid": ""
 *      "author": "wklin"
 *      "company": "OR_V8800"
 *      "description": "OR Injection Molding Machine V8800 Thing Model"
 *      "thing": {
 *           "version": "v1",
 *           "schema": "https://sm.sft.fii-icloud.com/thing-model-server/corepro_thing_schema.json",
 *           "profile": {
 *               "pid": "6456559484229801863"
 *           },
 *           "properties": [
 *               {
 *                   "identifier": "attribute1",
 *                   "display_name": "attribute1_display_name",
 *                   "operation": "rw",
 *                   "description": "descrption of attribute1",
 *                   "data_type": {
 *                       "type": "integer",
 *                       "min": 1,
 *                       "max": 10,
 *                       "unit": "cm"
 *                   }
 *               },
 *               {
 *                   "identifier": "attribute2",
 *                   "display_name": "attribute2_display_name",
 *                   "operation": "r",
 *                   "description": "description of attribute2",
 *                   "data_type": {
 *                       "type": "array",
 *                       "array_size": 1024,
 *                       "array_type": "integer"
 *                   }
 *               },
 *               {
 *                   "identifier": "attribute3",
 *                   "display_name": "attribute3_display_name",
 *                   "operation": "r",
 *                   "description": "description of attribute3",
 *                   "data_type": {
 *                       "type": "enum",
 *                       "enumeration": ["red", "green", "blue"]
 *                   }
 *               },
 *               {
 *                   "identifier": "attribute4",
 *                   "display_name": "attribute4_display_name",
 *                   "operation": "r",
 *                   "description": "description of attribute4",
 *                   "data_type": {
 *                       "type": "struct",
 *                       "struct_defs": [ 
 *                           {
 *                               "id": "name",
 *                               "type": "string"
 *                           },
 *                           {
 *                               "id": "birthday",
 *                               "type": "date"
 *                           }
 *                       ]
 *                   }
 *               }
 *           ],
 * 
 *           "services": [
 *               {
 *                   "identifier": "service1",
 *                   "display_name": "service1_display_name", 
 *                   "description": "description of service1",
 *                   "input_data": [
 *                       {
 *                           "identifier": "in_param1",
 *                           "description": "description of in_param1",
 *                           "display_name": "in_param1_display_name",
 *                           "data_type": {
 *                               "type": "integer",
 *                               "min": 1,
 *                               "max": 10,
 *                               "unit": "cm"
 *                           }
 *                       },
 *                       {
 *                           "identifier": "in_param2",
 *                           "display_name": "in_param2_display_name",
 *                           "description'": "description of in_param2",
 *                           "data_type": {
 *                               "type": "array",
 *                               "array_size": 16,
 *                               "array_type": "float"
 *                           }
 *                       }
 *                   ],
 *                   "output_data": [
 *                       {
 *                           "identifier": "out_param",
 *                           "display_name": "out_param_display_name",
 *                           "description": "description of out_param",
 *                           "data_type": {
 *                               "type": "float",
 *                               "min": 2.0,
 *                               "max": 5.0,
 *                               "unit": "inch"
 *                           }
 *                       }
 *                   ]
 *               }
 *           ],
 *           "events": [
 *               {
 *                   "identifier": "event1",
 *                   "display_name": "event1_display_name",
 *                   "description": "description of event1",
 *                   "event_type": "information",
 *                   "output_data": [
 *                       {
 *                           "identifier": "out_param1",
 *                           "display_name": "out_param1_display_name",
 *                           "description": "description of out_param1",
 *                           "data_type": {
 *                               "type": "integer",
 *                               "min": 10,
 *                               "max": 100,
 *                               "unit": "pa"
 *                           }
 *                       },
 *                       {
 *                           "identifier": "out_param2",
 *                           "display_name": "out_param2_display_name",
 *                           "description": "description of out_param2",
 *                           "data_type": {
 *                               "type": "string"
 *                           }
 *                       }
 *                   ]
 *               }
 *           ],
 *           "device_info": [
 *               {
 *                   "device_info_id": "status",
 *                   "device_info_type": {
 *                       "type": "enum",
 *                       "enumeration": [0, 1, 2]
 *                   }
 *               },
 *               {
 *                   "device_info_id": "cpu",
 *                   "device_info_type": {
 *                       "type": "integer",
 *                       "min": 0,
 *                       "max": 100
 *                   }
 *               },
 *               {
 *                   "device_info_id": "memory",
 *                   "device_info_type": {
 *                       "type": "integer",
 *                       "min": 0,
 *                       "unit": "MB"
 *                   }
 *               },
 *               {
 *                   "device_info_id": "disk",
 *                   "device_info_type": {
 *                       "type": "integer",
 *                       "min": 0,
 *                       "unit": "GB"
 *                   }
 *               }
 *           ]
 *        }
 *    }
 *
 * @apiHeader {string} Authorization The Bearer Token.
 * @apiHeader {string} Content-Type The media type of the resource. It should be "application/json".
 *
 * @apiParam {object} basic_info Basic thing model info.
 * @apiParam {object} thing_model Model data, a JSON object with thing model information.
 * @apiParam {string} thing_model.version thing model version
 * @apiParam {string} thing_model.schema thing model schema
 * @apiParam {string} thing_model.profile product profile
 * @apiParam {object[]} thing_model.properties properties of the thing model
 * @apiParam {object[]} thing_model.services services of the thing model
 * @apiParam {object[]} thing_model.events events of the thing model
 * @apiParam {object[]} thing_model.device_info device info of the product
 * @apiSuccess (Success 201) 201 If successful, this method returns product id in the response body.
 * @apiSuccess (Success 201) {string} 201.pid product identifier.
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 201 Created
 *    Content-Type: application/json
 *    {
 *       "pid": "6456559484229801863"
 *    }
 *
 * @apiUse BadRequestError
 * @apiUse UnauthorizedError
 * @apiUse PermissionDeniedError
 */

/**
 * @api {put} /v1/things/{pid} Update Thing Model
 * @apiVersion 1.0.0
 * @apiName updateThing
 * @apiGroup thing
 * @apiPermission MANAGE_THING
 * @apiDescription
 * Update a thing model specified by {pid}.
 *
 * @apiExample {http} Example usage:
 *    PUT /v1/things/6456559484229801863
 *    Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6.....
 *    Content-Type: application/json
 *    {
 *       "author": "WilliamL"
 *       "company": "A company"
 *       "thing": {
 *           "version": "v1",
 *           "schema": "https://corepro.fiibeacon.com/corepro_thing_schema.json",
 *           "properties": [
 *               {
 *                   "identifier": "attribute1",
 *                   "display_name": "attribute1_display_name",
 *                   "operation": "rw",
 *                   "description": "descrption of attribute1",
 *                   "data_type": {
 *                       "type": "integer",
 *                       "min": 1,
 *                       "max": 10,
 *                       "unit": "cm"
 *                   }
 *               },
 *               {
 *                   "identifier": "attribute2",
 *                   "display_name": "attribute2_display_name",
 *                   "operation": "r",
 *                   "description": "description of attribute2",
 *                   "data_type": {
 *                       "type": "array",
 *                       "array_size": 1024,
 *                       "array_type": "integer"
 *                   }
 *               },
 *               {
 *                   "identifier": "attribute3",
 *                   "display_name": "attribute3_display_name",
 *                   "operation": "r",
 *                   "description": "description of attribute3",
 *                   "data_type": {
 *                       "type": "enum",
 *                       "enumeration": ["red", "green", "blue"]
 *                   }
 *               },
 *               {
 *                   "identifier": "attribute4",
 *                   "display_name": "attribute4_display_name",
 *                   "operation": "r",
 *                   "description": "description of attribute4",
 *                   "data_type": {
 *                       "type": "struct",
 *                       "struct_defs": [ 
 *                           {
 *                               "id": "name",
 *                               "type": "string"
 *                           },
 *                           {
 *                               "id": "birthday",
 *                               "type": "date"
 *                           }
 *                       ]
 *                   }
 *               }
 *           ],
 * 
 *           "services": [
 *               {
 *                   "identifier": "service1",
 *                   "display_name": "service1_display_name", 
 *                   "description": "description of service1",
 *                   "input_data": [
 *                       {
 *                           "identifier": "in_param1",
 *                           "description": "description of in_param1",
 *                           "display_name": "in_param1_display_name",
 *                           "data_type": {
 *                               "type": "integer",
 *                               "min": 1,
 *                               "max": 10,
 *                               "unit": "cm"
 *                           }
 *                       },
 *                       {
 *                           "identifier": "in_param2",
 *                           "display_name": "in_param2_display_name",
 *                           "description'": "description of in_param2",
 *                           "data_type": {
 *                               "type": "array",
 *                               "array_size": 16,
 *                               "array_type": "float"
 *                           }
 *                       }
 *                   ],
 *                   "output_data": [
 *                       {
 *                           "identifier": "out_param",
 *                           "display_name": "out_param_display_name",
 *                           "description": "description of out_param",
 *                           "data_type": {
 *                               "type": "float",
 *                               "min": 2.0,
 *                               "max": 5.0,
 *                               "unit": "inch"
 *                           }
 *                       }
 *                   ]
 *               }
 *           ],
 *           "events": [
 *               {
 *                   "identifier": "event1",
 *                   "display_name": "event1_display_name",
 *                   "description": "description of event1",
 *                   "event_type": "information",
 *                   "output_data": [
 *                       {
 *                           "identifier": "out_param1",
 *                           "display_name": "out_param1_display_name",
 *                           "description": "description of out_param1",
 *                           "data_type": {
 *                               "type": "integer",
 *                               "min": 10,
 *                               "max": 100,
 *                               "unit": "pa"
 *                           }
 *                       },
 *                       {
 *                           "identifier": "out_param2",
 *                           "display_name": "out_param2_display_name",
 *                           "description": "description of out_param2",
 *                           "data_type": {
 *                               "type": "string"
 *                           }
 *                       }
 *                   ]
 *               }
 *           ],
 *           "device_info": [
 *               {
 *                   "device_info_id": "status",
 *                   "device_info_type": {
 *                       "type": "enum",
 *                       "enumeration": [0, 1, 2]
 *                   }
 *               },
 *               {
 *                   "device_info_id": "cpu",
 *                   "device_info_type": {
 *                       "type": "integer",
 *                       "min": 0,
 *                       "max": 100
 *                   }
 *               },
 *               {
 *                   "device_info_id": "memory",
 *                   "device_info_type": {
 *                       "type": "integer",
 *                       "min": 0,
 *                       "unit": "MB"
 *                   }
 *               },
 *               {
 *                   "device_info_id": "disk",
 *                   "device_info_type": {
 *                       "type": "integer",
 *                       "min": 0,
 *                       "unit": "GB"
 *                   }
 *               }
 *           ]
 *        }
 *    }
 *
 * @apiHeader {string} Authorization The Bearer Token. It's "Bearer " + Access JSON Web Token.
 * @apiHeader {string} Content-Type The media type of the resource. It should be "application/json".
 *
 * @apiParam {string} product_id Associated device type id.
 * @apiParam {object} thing_model Model data, a JSON object with thing model information.
 * @apiParam {string} thing_model.version thing model version
 * @apiParam {string} thing_model.schema thing model schema
 * @apiParam {string} thing_model.profile product profile
 * @apiParam {object[]} thing_model.properties properties of the thing model
 * @apiParam {object[]} thing_model.services services of the thing model
 * @apiParam {object[]} thing_model.events events of the thing model
 * @apiParam {object[]} thing_model.device_info device info of the product
 *
 * @apiUse NoContentSuccess
 *
 * @apiUse BadRequestError
 * @apiUse UnauthorizedError
 * @apiUse PermissionDeniedError
 */

/**
 * @api {delete} /v1/things/{pid} Delete Thing Model
 * @apiVersion 1.0.0
 * @apiName deleteThing
 * @apiGroup thing
 * @apiPermission MANAGE_THING
 * @apiDescription
 * Delete a thing model.
 *
 * @apiExample {http} Example usage:
 *    DELETE /v1/things/6456559484229801863
 *    Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6.....
 *    Content-Type: application/json
 *
 * @apiHeader {string} Authorization The Bearer Token.
 * @apiHeader {string} Content-Type The media type of the resource. It should be "application/json".
 *
 * @apiParam {string} pid pid
 *
 * @apiUse NoContentSuccess
 *
 * @apiUse BadRequestError
 * @apiUse UnauthorizedError
 * @apiUse PermissionDeniedError
 */

